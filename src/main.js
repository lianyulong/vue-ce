import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
Vue.config.productionTip = false;
import echarts from 'echarts'
Vue.prototype.$echarts = echarts
Vue.filter('time', function(msg) {
	var date = new Date(msg);
	var y = date.getFullYear();
	var MM = date.getMonth() + 1;
	MM = MM < 10 ? ('0' + MM) : MM;
	var d = date.getDate();
	d = d < 10 ? ('0' + d) : d;
	return y + '-' + MM + '-' + d;
})
new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
