import axios from "axios"
const baseURL = process.env.VUE_APP_BASE_API?process.env.VUE_APP_BASE_API:"http://47.93.235.96:8888/api/private/v1/";
const instance = axios.create({
	baseURL: baseURL
})

import store from "../store/index.js"
import router from "../router/index.js"
import {
	Message
} from "element-ui"
instance.interceptors.request.use(config => {
	if (store.state.token) {
		config.headers["Authorization"] = store.state.token
	}
	return config;
}, error => {
	return Promise.reject(error);
});
instance.interceptors.response.use(response => {
	if (response.data.meta.msg === '无效token') {
		router.push("/login")
	}
	return response;
}, error => {
	return Promise.reject(error);
});
export function http(url, method, data, params) {
	return new Promise((resolve, reject) => {
		instance({
				url,
				method,
				data,
				params
			})
			.then(res => {
				if ((res.status >= 200 && res.status < 300) || res.status === 304) {
					if (res.data.meta.status === 200 || (res.data.meta.status >= 200 && res.status < 300) || res.data.meta.status ===
						304) {
						resolve(res.data)
					} else {
						reject(res.data.meta)
						Message({
							message: res.data.meta.msg,
							type: 'error'
						});
					}
				} else {
					reject(res.data.meta)
					Message({
						message: res.data.meta.msg,
						type: 'error'
					});
				}
			}).catch(err => {
				reject(err.data.meta)
			})
	})
}
export const uploadURL = baseURL + "upload"
