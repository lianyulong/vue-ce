import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
	return originalPush.call(this, location).catch(err => err)
};
const routes = [{
		path: '/',
		redirect: "/login"
	},
	{
		path: '/login',
		name: 'login',
		component: () => import( /* webpackChunkName: "login" */ '../views/Login.vue')
	},
	{
		path: '/index',
		name: 'index',
		component: () => import( /* webpackChunkName: "index" */ '../views/Index.vue'),
		children: [{
				path: '/',
				redirect: "users"
			}, {
				path: 'users',
				name: 'users',
				component: () => import( /* webpackChunkName: "users" */ '../components/users/Users.vue'),
			},
			{
				path: 'roles',
				name: 'roles',
				component: () => import( /* webpackChunkName: "roles" */ '../components/roles/Roles.vue'),
			},
			{
				path: 'rights',
				name: 'rights',
				component: () => import( /* webpackChunkName: "rights" */ '../components/rights/Rights.vue'),
			},
			{
				path: 'goods',
				name: 'goods',
				component: () => import( /* webpackChunkName: "goods" */ '../components/goods/Goods.vue'),
				children: [{
						path: '/',
						redirect: "goodlist"
					},
					{
						path: 'goodlist',
						name: 'goodlist',
						component: () => import( /* webpackChunkName: "goodlist" */ '../components/goods/GoodList.vue'),
					},
					{
						path: 'addgoods',
						name: 'addgoods',
						component: () => import( /* webpackChunkName: "addgoods" */ '../components/goods/AddGoods.vue'),
					},
					{
						path: 'editgood',
						name: 'editgood',
						component: () => import( /* webpackChunkName: "editgood" */ '../components/goods/EditGood.vue'),
					}
				]
			},
			{
				path: 'params',
				name: 'params',
				component: () => import( /* webpackChunkName: "params" */ '../components/params/Params.vue'),
			},
			{
				path: 'categories',
				name: 'categories',
				component: () => import( /* webpackChunkName: "categories" */ '../components/categories/Categories.vue'),
			},
			{
				path: 'orders',
				name: 'orders',
				component: () => import( /* webpackChunkName: "orders" */ '../components/orders/Orders.vue'),
			},
			{
				path: 'reports',
				name: 'reports',
				component: () => import( /* webpackChunkName: "reports" */ '../components/reports/Reports.vue'),
			},
		]
	}
]

const router = new VueRouter({
	routes
})

export default router
